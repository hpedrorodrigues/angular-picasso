'use strict';

var app = angular.module('app', ['angular-picasso']);

app.controller('AppController', function () {
  this.images = [
    "http://www.hdwallpapers.in/walls/angry_birds_2-wide.jpg",
    "http://www.hdwallpapers.in/thumbs/scalebound_2016_game-t1.jpg",
    "http://www.hdwallpapers.in/thumbs/zenvo_st1_2015-t1.jpg",
    "http://www.hdwallpapers.in/thumbs/minions_new-t1.jpg",
    "http://www.hdwallpapers.in/thumbs/2015_ares_design_lamborghini_huracan-t1.jpg",
    "http://www.hdwallpapers.in/thumbs/melissa_benoist_supergirl-t1.jpg"
  ];
});