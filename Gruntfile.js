'use strict';

module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      options: {
        jshintrc: true,
        reporter: 'checkstyle',
        reporterOutput: 'reporter/jshint-result.xml'
      },
      files: ['Gruntfile.js', 'src/**/*.js']
    },
    uglify: {
      options: {
        banner: '' +
        '/*! \n' +
        ' <%= pkg.name %> - v<%= pkg.version %> \n' +
        ' <%= pkg.author %> \n' +
        ' <%= grunt.template.today("yyyy-mm-dd") %> \n' +
        '*/\n',
        compress: {
          dead_code: true
        }
      },
      picasso: {
        options: {
          beautify: {
            width: 80,
            beautify: false
          }
        },
        files: {
          'dist/angular-picasso.min.js': ['src/angular-picasso.js']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('default', ['jshint', 'uglify']);
};