# Angular Picasso

A simple library to make it easy to pre-load images.

Inspired by [Square](https://github.com/square) with [Picasso](http://square.github.io/picasso/).

## Installation

### Manual download

See my [releases](https://github.com/hpedrorodrigues/angular-picasso/releases) page for download.

### Bower

```bash
bower install angular-picasso
```

### NPM

```bash
npm install --save angular-picasso
```

## Load

Directly by HTML including script files.

```html
<script src="node_modules/angular/angular.min.js"></script>
<script src="node_modules/angular-picasso/dist/angular-picasso.min.js"></script>
```

## Add module dependency

```javascript
angular.module('app', ['angular-picasso']);
```

## Usage

```html
<img picasso ng-src="url" loading="url" error="url" />
```

See [index.html](./demo/index.html) for more details.

## Issues

For questions, bug reports, improve documentation, and feature request please
search through existing
[issue](https://github.com/hpedrorodrigues/angular-picasso/issues) and if you don't
find and answer open a new one [here](https://github.com/hpedrorodrigues/angular-picasso/issues/new).
If you need support send me an [email](mailto:hs.pedro.rodrigues@gmail.com). You can also
contact [me](https://github.com/hpedrorodrigues) for any non public concerns.

## Contribution

When issuing a pull request, please exclude changes in the **dist** folder to
avoid merge conflicts.

## License

Angular Picasso is released under the MIT license. See [LICENSE](./LICENSE) for details.

## More

Angular Picasso is a work in progress, feel free to improve it.